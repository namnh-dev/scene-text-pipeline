import os
from tqdm import tqdm
from PIL import Image

root_folder = '/mlcv/Databases/VinText/vietnamese_original/vietnamese/vin_txt'
root_images = '/mlcv/Databases/VinText/vietnamese/vietnamese/merge_images'

target_folder = '../data_new/VinText/'

list_files = sorted(os.listdir(root_folder))
list_images = sorted(os.listdir(root_images))

def get_yolo_format(size, box):
    print(box)
    xmin = int(min(box[::2]))
    ymin = int(min(box[1::2]))
    xmax = int(max(box[::2]))
    ymax = int(max(box[1::2]))
    print('xmin, ymin, xmax, ymax', xmin, ymin, xmax, ymax)
    x_center = int((xmin+xmax)/2)
    y_center = int((ymin+ymax)/2)
    w = xmax - xmin
    h = ymax - ymin
    print('w,h',size[0],size[1])
    dw = 1/size[0]
    dh = 1/size[1]

    return x_center*dw, y_center*dh, w*dw, h*dh

for i, file_ in tqdm(enumerate(list_files)):
    
    images = Image.open(os.path.join(root_images, list_images[i]))
    size = (images.size[0], images.size[1])
    rows = open(os.path.join(root_folder, file_),'r',encoding='utf-8').readlines()
    
    #if len(rows) <= 0: continue

    target_files = open(os.path.join(target_folder, file_),'w+',encoding='utf8')
    if len(rows) == 0: continue
    for row_ in rows:
        row = row_[:-1]
        label = row.split(',',8)[-1]
        points = row.split(',',8)[:-1]
        
        box = []
        try:
            for p in points:
                 box.append(int(p))
        except:
            continue

        x,y,w,h = get_yolo_format(size, box)
        
        if label == '###':
            label = 0
        else: label = 1
        line = str(label)+' '+str(x)+' '+str(y)+' '+str(w)+' '+str(h)+'\n'
        target_files.write(line)

