from vietocr.tool.config import Cfg
from vietocr.model.trainer import Trainer



def main():
    config = Cfg.load_config_from_name('vgg_transformer')


    dataset_params = {
        'name': 'Vintext',
        'data_root':'/mlcv/WorkingSpace/SceneText/namnh/Text_Reg/vedastr/DataCrop/VinText',
        #'data_root':'../vedastr/DataCrop/VinText',
        'train_annotation':'train_labels.txt',
        'valid_annotation':'val_labels.txt'
    }

    params = {
        'print_every':200,
        'valid_every':2000,
        'iters':200000,
        'batch_size': 256,
        #'checkpoint':'./weights/vggseq2seq_ocr_10kgeneratedword1_2.pth',    
        'export':'./weights/vggtransformer_VinText.pth',
        'metrics': 5000
    }

    config['trainer'].update(params)
    config['dataset'].update(dataset_params)
    config['device'] = 'cuda:4'

    trainer = Trainer(config, pretrained=False)

    #trainer.config.save('my_config/vgg_transformer.yml')

    #trainer.load_checkpoint('./weights/vggseq2seq_ocr_10kgeneratedword1_2.pth')
    #trainer.load_weights('./weights/vggseq2seq_ocr_10kgeneratedword1_2.pth')
    trainer.train()
    
    #trainer.visualize_prediction()
    print(trainer.precision())
 



if __name__ == '__main__':
    main()
